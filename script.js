//Form variables
const form = document.getElementById('form');
const username = document.getElementById('username');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

// function
function showError(input, message) {
    // outline the input with red we need to get the parent
    const formControl = input.parentElement;// gets the parents
    formControl.className = 'form-control error';// small is now visible per css

    const small = formControl.querySelector('small'); // get the small in teh parent el div
    small.innerText = message;
}

function showSuccess(input) {
    // outline the input with red we need to get the parent
    const formControl = input.parentElement;// gets the parents
    formControl.className = 'form-control success';// small is now visible per css
}

function checkEmail(input) {
  //returns true or false
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(re.test(input.value.trim())){
    showSuccess(input);
  }else {
    showError(input, 'Email is not valid');
  }
  return re.test(String(email).toLowerCase());
}

function checkRequired(inputArr) {
  //loop through the array and check on each one
  // user a higher order array methods
  inputArr.forEach(function(input) {
    if(input.value.trim() === '') {
      showError(input, `${getFieldName(input)} is required`);
    } else {
      showSuccess(input);
    }
  });
}

//check length
function checkLength(input, min, max){
  if (input.value.length < min) {
    showError(input, `${getFieldName(input)} must be at least ${min} characters`);
  } else if (input.value.length > max)
  {
    showError(input, `${getFieldName(input)} must be less than ${max} characters`);
  }
  else {
    showSuccess(input);
  }
}

function checkPasswordsMatch(input1, input2){
  if (input1.value !== input2.value) showError(input2, 'Passwords do not match');
}

function getFieldName(input) {
  return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}

//Add event listener
form.addEventListener('submit', (e) =>{
  e.preventDefault();
  checkRequired([username, email, password, password2])
  checkLength(username, 3, 15);
  checkLength(password, 6, 25);
  checkEmail(email);
  checkPasswordsMatch(password, password2)
});


